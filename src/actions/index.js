import * as loginPage from "./loginPage";
import * as dashboardPage from "./dashboardPage";

export default {
  ...loginPage,
  ...dashboardPage
};
