import { index } from "api/admin";
import history from "history.js";

export const ADMIN_AUTHENTICATION_SUCCESS = "ADMIN_AUTHENTICATION_SUCCESS";
export const ADMIN_AUTHENTICATION_FAIL = "ADMIN_AUTHENTICATION_FAIL";

export function adminAuthorization(login, password) {
  return async (dispatch, getState) => {
    try {
      await index(login, password);

      sessionStorage.setItem("authorized", true);
      sessionStorage.setItem("login", login);
      sessionStorage.setItem("password", password);

      await dispatch({
        type: ADMIN_AUTHENTICATION_SUCCESS,
        authError: false,
        authorizedStatus: true
      });

      await history.push("/dashboard");
    } catch (e) {
      sessionStorage.removeItem("authorized");
      sessionStorage.removeItem("login");
      sessionStorage.removeItem("password");

      dispatch({
        type: ADMIN_AUTHENTICATION_FAIL,
        authError: true
      });
    }
  };
}
