import { getUsers, getUserByPhone, deleteUserByPhone, index } from "api/admin";

export const USERS_LOADING = "USERS_LOADING";
export const USERS_LOAD_SUCCESS = "USERS_LOAD_SUCCESS";
export const USERS_LOAD_FAIL = "USERS_LOAD_FAIL";
export const USER_LOADING = "USER_LOADING";
export const USER_LOAD_SUCCESS = "USER_LOAD_SUCCESS";
export const USER_LOAD_FAIL = "USER_LOAD_FAIL";
export const DELETE_USER_SUCCESS = "DELETE_USER_SUCCESS";
export const DELETE_USER_FAIL = "DELETE_USER_FAIL";
export const LOGOUT = "LOGOUT";
export const OPEN_USER_MODAL = "OPEN_USER_MODAL";
export const CLOSE_USER_MODAL = "CLOSE_USER_MODAL";
export const SHOW_ALL_USERS = "SHOW_ALL_USERS";
export const SHOW_ONE_USER = "SHOW_ONE_USER";
export const OPEN_USER_DELETE_MODAL = "OPEN_USER_DELETE_MODAL";
export const CLOSE_USER_DELETE_MODAL = "CLOSE_USER_DELETE_MODAL";

const login = sessionStorage.getItem("login");
const password = sessionStorage.getItem("password");

export function loadUsers() {
  return async (dispatch, getState) => {
    dispatch({
      type: USERS_LOADING,
      loading: true
    });

    const result = await getUsers();

    if (result.status === 200) {
      const users = result.data.users;

      dispatch({
        type: USERS_LOAD_SUCCESS,
        loading: false,
        users
      });

      return;
    }

    if (result.status === 401) {
      await index(login, password);
      const result = await getUsers();

      const users = result.data.users;

      dispatch({
        type: USERS_LOAD_SUCCESS,
        loading: false,
        users
      });
      return;
    }

    dispatch({
      type: USERS_LOAD_FAIL,
      loading: false
    });

    return;
  };
}

export function loadUserByPhone(phone) {
  return async (dispatch, getState) => {
    dispatch({
      type: USER_LOADING,
      loading: true
    });

    const formatedPhone = phone.replace(/\+/g, "");

    const result = await getUserByPhone(formatedPhone);

    if (result.status === 200) {
      const user = result.data;

      dispatch({
        type: USER_LOAD_SUCCESS,
        loading: false,
        user
      });

      return;
    }

    dispatch({
      type: USER_LOAD_FAIL,
      loading: false
    });
  };
}

export function deleteUser(phone) {
  return async (dispatch, getState) => {
    const formatedPhone = phone.replace(/\+/g, "");

    const result = await deleteUserByPhone(formatedPhone);

    if (result.status === 200) {
      dispatch({
        type: DELETE_USER_SUCCESS
      });

      dispatch(loadUsers());

      return;
    }

    dispatch({
      type: DELETE_USER_FAIL
    });
  };
}

export function logOut() {
  return dispatch => {
    sessionStorage.removeItem("authorized");
    sessionStorage.removeItem("login");
    sessionStorage.removeItem("password");

    dispatch({
      type: LOGOUT,
      authorizedStatus: false
    });
  };
}

export function openModal(phone) {
  return async (dispatch, getState) => {
    dispatch(loadUserByPhone(phone));

    dispatch({
      type: OPEN_USER_MODAL,
      showUserDetail: true
    });
  };
}

export function closeModal() {
  return async (dispatch, getState) => {
    dispatch({
      type: CLOSE_USER_MODAL,
      showUserDetail: false
    });
  };
}

export function showAllUsers() {
  return async (dispatch, getState) => {
    dispatch({
      type: SHOW_ALL_USERS,
      allUsers: true
    });
  };
}

export function showOneUser() {
  return async (dispatch, getState) => {
    dispatch({
      type: SHOW_ONE_USER,
      allUsers: false
    });
  };
}

export function openDeleteModal(phone) {
  return async (dispatch, getState) => {
    dispatch({
      type: OPEN_USER_DELETE_MODAL,
      showDeleteModal: true,
      selectedUser: phone
    });
  };
}

export function closeDeleteModal() {
  return async (dispatch, getState) => {
    dispatch({
      type: CLOSE_USER_DELETE_MODAL,
      showDeleteModal: false
    });
  };
}
