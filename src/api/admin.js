import axios from "axios";

const link = process.env.REACT_APP_HOST;

export async function index(login, pass) {
  await axios({
    url: `${link}/admin/login`,
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Cache: "no-cache"
    },
    data: {
      login: login,
      password: pass
    },
    withCredentials: true
  }).then(response => {
    return {
      ...response
    };
  });
}

export const getUsers = async function() {
  try {
    return await axios({
      url: `${link}/admin/users`,
      method: "GET",
      withCredentials: true
    }).then(response => {
      return {
        ...response
      };
    });
  } catch (e) {
    return e.response;
  }
};

export const getUserByPhone = async function(phone) {
  try {
    return await axios({
      url: `${link}/admin/user/+${phone}`,
      method: "GET",
      withCredentials: true
    }).then(response => {
      return {
        ...response
      };
    });
  } catch (e) {
    return e.response;
  }
};

export const deleteUserByPhone = async function(phone) {
  try {
    return await axios({
      url: `${link}/admin/user/+${phone}`,
      method: "DELETE",
      withCredentials: true
    }).then(response => {
      return {
        ...response
      };
    });
  } catch (e) {
    return e.response;
  }
};
