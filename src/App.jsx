import React, { Component } from "react";
import { connect } from "react-redux";
import { Route, Router, Redirect } from "react-router-dom";
import actions from "actions";
import LoginPage from "containers/LoginPage";
import DashboardPage from "containers/DashboardPage";
import "App.css";
import history from "history.js";

class App extends Component {
  render() {
    return (
      <Router history={history}>
        <div className="App">
          <Route exact path="/" component={LoginPage} />
          <Route
            path="/dashboard"
            render={() =>
              Boolean(sessionStorage.getItem("authorized")) ? (
                <DashboardPage />
              ) : (
                <Redirect to="/" />
              )
            }
          />
        </div>
      </Router>
    );
  }
}

function mapStateToProps({ loginPage }) {
  return {
    loginPage
  };
}

const mapDispatchToProps = dispatch => ({
  adminAuthorization: () => dispatch(actions.adminAuthorization())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
