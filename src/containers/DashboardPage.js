import { connect } from "react-redux";

import DashboardPage from "pages/DashboardPage";
import actions from "actions";

function mapStateToProps({ dashboardPage }) {
  return {
    dashboardPage
  };
}

const mapDispatchToProps = dispatch => ({
  loadUsers: () => dispatch(actions.loadUsers()),
  changePhoneValue: value => dispatch(actions.changePhoneValue(value)),
  loadUserByPhone: phone => dispatch(actions.loadUserByPhone(phone)),
  deleteUser: phone => dispatch(actions.deleteUser(phone)),
  openModal: phone => dispatch(actions.openModal(phone)),
  closeModal: () => dispatch(actions.closeModal()),
  openDeleteModal: phone => dispatch(actions.openDeleteModal(phone)),
  closeDeleteModal: () => dispatch(actions.closeDeleteModal()),
  showOneUser: () => dispatch(actions.showOneUser()),
  showAllUsers: () => dispatch(actions.showAllUsers()),
  logOut: () => dispatch(actions.logOut())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardPage);
