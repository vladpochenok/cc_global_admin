import { connect } from "react-redux";

import LoginPage from "pages/LoginPage.jsx";
import actions from "actions";

function mapStateToProps({ loginPage }) {
  return {
    loginPage
  };
}

const mapDispatchToProps = dispatch => ({
  adminAuthorization: (login, password) =>
    dispatch(actions.adminAuthorization(login, password))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);
