import { combineReducers } from "redux";

import loginPage from "./loginPage";
import dashboardPage from "./dashboardPage";

const rootReducer = combineReducers({
  loginPage,
  dashboardPage
});

export default rootReducer;
