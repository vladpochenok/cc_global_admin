import produce from "immer";

import {
  ADMIN_AUTHENTICATION_SUCCESS,
  ADMIN_AUTHENTICATION_FAIL
} from "actions/loginPage";

import { LOGOUT } from "actions/dashboardPage";

const DEFAULT_STATE = {
  authError: false,
  authorizedStatus: false
};

export default function loginPage(state = DEFAULT_STATE, action) {
  return produce(state, draft => {
    switch (action.type) {
      case ADMIN_AUTHENTICATION_SUCCESS:
        draft.authError = action.authError;
        draft.authorizedStatus = action.authorizedStatus;

        return;
      case ADMIN_AUTHENTICATION_FAIL:
        draft.authError = action.authError;

        return;
      case LOGOUT:
        draft.login = DEFAULT_STATE.login;
        draft.password = DEFAULT_STATE.password;
        draft.authorizedStatus = DEFAULT_STATE.authorizedStatus;

        return;
      default:
        return;
    }
  });
}
