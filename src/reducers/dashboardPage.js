import produce from "immer";

import {
  USERS_LOADING,
  USERS_LOAD_SUCCESS,
  USERS_LOAD_FAIL,
  USER_LOADING,
  USER_LOAD_SUCCESS,
  USER_LOAD_FAIL,
  OPEN_USER_MODAL,
  CLOSE_USER_MODAL,
  SHOW_ALL_USERS,
  SHOW_ONE_USER,
  OPEN_USER_DELETE_MODAL,
  CLOSE_USER_DELETE_MODAL,
  LOGOUT
} from "actions/dashboardPage";

const DEFAULT_STATE = {
  users: [],
  user: {},
  loading: false,
  phone: "",
  allUsers: true,
  showUserDetail: false,
  selectedUser: "",
  showDeleteModal: false
};

export default function dashboardPage(state = DEFAULT_STATE, action) {
  return produce(state, draft => {
    switch (action.type) {
      case USERS_LOADING:
        draft.loading = action.loading;

        return;
      case USERS_LOAD_SUCCESS:
        draft.users = action.users;
        draft.loading = action.loading;

        return;
      case USERS_LOAD_FAIL:
        draft.loading = action.loading;

        return;
      case USER_LOADING:
        draft.loading = action.loading;

        return;
      case USER_LOAD_SUCCESS:
        draft.user = action.user;
        draft.loading = action.loading;

        return;
      case USER_LOAD_FAIL:
        draft.loading = action.loading;
        draft.user = DEFAULT_STATE.user;

        return;
      case SHOW_ALL_USERS:
        draft.allUsers = action.allUsers;

        return;
      case SHOW_ONE_USER:
        draft.allUsers = action.allUsers;

        return;
      case OPEN_USER_MODAL:
        draft.showUserDetail = action.showUserDetail;

        return;

      case CLOSE_USER_MODAL:
        draft.showUserDetail = action.showUserDetail;

        return;
      case OPEN_USER_DELETE_MODAL:
        draft.showDeleteModal = action.showDeleteModal;
        draft.selectedUser = action.selectedUser;

        return;

      case CLOSE_USER_DELETE_MODAL:
        draft.showDeleteModal = action.showDeleteModal;
        draft.selectedUser = DEFAULT_STATE.selectedUser;

        return;
      case LOGOUT:
        draft.users = DEFAULT_STATE.users;
        draft.user = DEFAULT_STATE.user;
        draft.phone = DEFAULT_STATE.phone;
        draft.allUsers = DEFAULT_STATE.allUsers;
        draft.selectedUser = DEFAULT_STATE.selectedUser;

        return;
      default:
        return;
    }
  });
}
