import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
  Paper,
  Fab
} from "@material-ui/core";
import { Delete, Info } from "@material-ui/icons";

const styles = theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing.unit * 3,
    overflowX: "auto"
  },
  table: {
    minWidth: 700
  },
  fab: {
    margin: theme.spacing.unit
  },
  message: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2
  }
});

function UserTable(props) {
  const { classes, openModal, user, openDeleteModal } = props;
  const { email, emailVerified, phone, balance, currency } = user;

  return (
    <Fragment>
      {Object.keys(user).length === 0 ? (
        <Paper className={classes.message} elevation={1}>
          <Typography variant="h5" component="h3">
            User not found
          </Typography>
        </Paper>
      ) : (
        <Paper className={classes.root}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <TableCell>Email</TableCell>
                <TableCell align="right">EmailVerified</TableCell>
                <TableCell align="right">Phone</TableCell>
                <TableCell align="right">Balance</TableCell>
                <TableCell align="right">Currency</TableCell>
                <TableCell align="right" />
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow>
                <TableCell component="th" scope="row">
                  {email}
                </TableCell>
                <TableCell align="right">
                  {emailVerified ? "true" : "false"}
                </TableCell>
                <TableCell align="right">{phone}</TableCell>
                <TableCell align="right">{balance}</TableCell>
                <TableCell align="right">{currency}</TableCell>
                <TableCell align="right">
                  <Fab
                    size="small"
                    color="primary"
                    aria-label="Add"
                    className={classes.fab}
                    onClick={() => openModal(phone)}
                  >
                    <Info />
                  </Fab>
                  <Fab
                    size="small"
                    color="secondary"
                    aria-label="Edit"
                    className={classes.fab}
                    onClick={() => openDeleteModal(phone)}
                  >
                    <Delete />
                  </Fab>
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </Paper>
      )}
    </Fragment>
  );
}

UserTable.propTypes = {
  classes: PropTypes.object.isRequired,
  openModal: PropTypes.func.isRequired,
  openDeleteModal: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired
};

export default withStyles(styles)(UserTable);
