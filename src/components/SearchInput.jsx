import React, { Component } from "react";
import PropTypes from "prop-types";
import Select from "react-select";
import { withStyles } from "@material-ui/core/styles";
import { Typography, TextField, Paper, MenuItem } from "@material-ui/core";

const styles = theme => ({
  root: {
    flexGrow: 1,
    maxWidth: "450px",
    marginRight: "10px"
  },
  input: {
    display: "flex",
    padding: 0,
    color: "#ffffff!important"
  },
  valueContainer: {
    display: "flex",
    flexWrap: "wrap",
    flex: 1,
    alignItems: "center",
    overflow: "hidden",
    color: "#ffffff"
  },
  chip: {
    margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`
  },
  noOptionsMessage: {
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`
  },
  singleValue: {
    fontSize: 16,
    color: "#ffffff"
  },
  placeholder: {
    position: "absolute",
    left: 2,
    fontSize: 16,
    color: "#ffffff"
  },
  paper: {
    position: "absolute",
    zIndex: 1,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0
  },
  divider: {
    height: theme.spacing.unit * 2
  }
});

function NoOptionsMessage(props) {
  return (
    <Typography
      color="textSecondary"
      className={props.selectProps.classes.noOptionsMessage}
      {...props.innerProps}
    >
      {props.children}
    </Typography>
  );
}

function inputComponent({ inputRef, ...props }) {
  return <div ref={inputRef} {...props} />;
}

function Control(props) {
  return (
    <TextField
      fullWidth
      InputProps={{
        inputComponent,
        inputProps: {
          className: props.selectProps.classes.input,
          inputRef: props.innerRef,
          children: props.children,
          ...props.innerProps
        }
      }}
      {...props.selectProps.textFieldProps}
    />
  );
}

function Option(props) {
  return (
    <MenuItem
      buttonRef={props.innerRef}
      selected={props.isFocused}
      component="div"
      style={{
        fontWeight: props.isSelected ? 500 : 400
      }}
      {...props.innerProps}
    >
      {props.children}
    </MenuItem>
  );
}

function Placeholder(props) {
  return (
    <Typography
      color="textSecondary"
      className={props.selectProps.classes.placeholder}
      {...props.innerProps}
    >
      {props.children}
    </Typography>
  );
}

function SingleValue(props) {
  return (
    <Typography
      className={props.selectProps.classes.singleValue}
      {...props.innerProps}
    >
      {props.children}
    </Typography>
  );
}

function ValueContainer(props) {
  return (
    <div className={props.selectProps.classes.valueContainer}>
      {props.children}
    </div>
  );
}

function Menu(props) {
  return (
    <Paper
      square
      className={props.selectProps.classes.paper}
      {...props.innerProps}
    >
      {props.children}
    </Paper>
  );
}

const components = {
  Control,
  Menu,
  NoOptionsMessage,
  Option,
  Placeholder,
  SingleValue,
  ValueContainer
};

class SearchInput extends Component {
  state = {
    single: null
  };

  handleChange = name => value => {
    const { loadUserByPhone, showOneUser } = this.props;
    let phone;

    if (value) {
      phone = value.value;
    }

    this.setState({
      [name]: value
    });

    if (phone) {
      loadUserByPhone(phone);
      showOneUser();
    }
  };

  render() {
    const { classes, theme, users } = this.props;

    const options = users.map(user => ({
      value: user.phone,
      label: user.phone
    }));

    const selectStyles = {
      input: base => ({
        ...base,
        color: "#ffffff",
        "& input": {
          font: "inherit",
          color: "#ffffff"
        }
      })
    };

    return (
      <div className={classes.root}>
        <Select
          classes={classes}
          styles={selectStyles}
          options={options}
          components={components}
          value={this.state.single}
          onChange={this.handleChange("single")}
          placeholder="Search user by phone"
          isClearable
        />
      </div>
    );
  }
}

SearchInput.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  showOneUser: PropTypes.func.isRequired,
  users: PropTypes.array.isRequired,
  loadUserByPhone: PropTypes.func.isRequired
};

export default withStyles(styles, { withTheme: true })(SearchInput);
