import React from "react";
import PropTypes from "prop-types";
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  withMobileDialog
} from "@material-ui/core";

class DeleteUserModal extends React.Component {
  handleClose = () => {
    const { closeModal } = this.props;

    closeModal();
  };

  handleUserDeletion = () => {
    const { deleteUser, closeModal, selectedUser } = this.props;

    deleteUser(selectedUser);
    closeModal();
  };

  render() {
    const { fullScreen, open } = this.props;

    return (
      <div>
        <Dialog
          fullScreen={fullScreen}
          open={open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">
            {"Are you sure you want to delete the user?"}
          </DialogTitle>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              No
            </Button>
            <Button onClick={this.handleUserDeletion} color="primary" autoFocus>
              Yes
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

DeleteUserModal.propTypes = {
  fullScreen: PropTypes.bool.isRequired,
  deleteUser: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
  selectedUser: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired
};

export default withMobileDialog()(DeleteUserModal);
