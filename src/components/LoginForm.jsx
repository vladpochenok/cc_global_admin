import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import classNames from "classnames";
import PropTypes from "prop-types";
import {
  Button,
  Typography,
  IconButton,
  InputAdornment,
  TextField,
  Paper
} from "@material-ui/core";
import { Visibility, VisibilityOff, Person } from "@material-ui/icons";

const styles = theme => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    width: "100vw",
    height: "100vh",
    justifyContent: "center",
    alignItems: "center"
  },
  button: {
    margin: theme.spacing.unit
  },
  margin: {
    margin: theme.spacing.unit
  },
  textField: {},
  icon: {
    margin: theme.spacing.unit * 2
  },
  paper: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    display: "flex",
    flexDirection: "column"
  },
  passwordInput: {
    paddingRight: 0
  },
  errorMessage: {
    color: "#ff0000",
    margin: "0 auto"
  }
});

class LoginForm extends Component {
  state = {
    login: "",
    password: "",
    showPassword: false
  };

  handleChange = prop => event => {
    this.setState({ [prop]: event.target.value });
  };

  handleClickShowPassword = () => {
    this.setState(state => ({ showPassword: !state.showPassword }));
  };

  handleSubmit = () => {
    const { authorization } = this.props;
    const { login, password } = this.state;

    if (!login || !password) {
      return;
    }

    authorization(login, password);
  };

  handleLoginChange = event => {
    this.setState({
      login: event.target.value
    });
  };

  handlePasswordChange = event => {
    this.setState({
      password: event.target.value
    });
  };

  render() {
    const { classes, loginPage } = this.props;
    const { authError } = loginPage;

    return (
      <form autoComplete="off" className={classes.root}>
        <Paper className={classes.paper} elevation={1}>
          <TextField
            error={authError ? true : false}
            id="login"
            className={classNames(classes.margin, classes.textField)}
            variant="outlined"
            label="Login"
            value={this.state.login}
            onChange={e => this.handleLoginChange(e)}
            InputProps={{
              endAdornment: (
                <InputAdornment variant="filled" position="end">
                  <IconButton>
                    <Person />
                  </IconButton>
                </InputAdornment>
              )
            }}
          />
          <TextField
            error={authError ? true : false}
            id="outlined-adornment-password"
            className={classNames(
              classes.margin,
              classes.textField,
              classes.passwordInput
            )}
            variant="outlined"
            type={this.state.showPassword ? "text" : "password"}
            label="Password"
            value={this.state.password}
            onChange={e => this.handlePasswordChange(e)}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    aria-label="Toggle password visibility"
                    onClick={this.handleClickShowPassword}
                  >
                    {this.state.showPassword ? (
                      <VisibilityOff color="primary" />
                    ) : (
                      <Visibility />
                    )}
                  </IconButton>
                </InputAdornment>
              )
            }}
          />
          {authError && (
            <Typography
              variant="button"
              gutterBottom
              className={classNames(classes.errorMessage)}
            >
              Incorrect login or password
            </Typography>
          )}
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={this.handleSubmit}
          >
            Sign In
          </Button>
        </Paper>
      </form>
    );
  }
}

LoginForm.propTypes = {
  loginPage: PropTypes.object.isRequired
};

export default withStyles(styles)(LoginForm);
