import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { AppBar, Toolbar, Typography, Button } from "@material-ui/core";
import { ExitToApp } from "@material-ui/icons";
import { fade } from "@material-ui/core/styles/colorManipulator";
import SearchInput from "components/SearchInput";

const styles = theme => ({
  root: {
    flexGrow: 1
  },
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  },
  rightIcon: {
    marginLeft: theme.spacing.unit
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.25)
    },
    marginLeft: 0,
    marginRight: "15px",
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing.unit,
      width: "auto"
    }
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  searchInput: {
    padding: "2px 4px",
    display: "flex",
    alignItems: "center",
    width: 260,
    marginRight: "15px"
  },
  input: {
    marginLeft: 8,
    flex: 1
  },
  iconButton: {
    padding: 10
  }
});

class Header extends Component {
  handleLogout = () => {
    const { logOut, history } = this.props;

    logOut();

    history.push("/");
  };

  render() {
    const { classes, users, loadUserByPhone, showOneUser } = this.props;
    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6" color="inherit" className={classes.grow}>
              Dashboard
            </Typography>
            <SearchInput
              users={users}
              loadUserByPhone={loadUserByPhone}
              showOneUser={showOneUser}
            />
            <Button color="inherit" onClick={this.handleLogout}>
              Logout
              <ExitToApp className={classes.rightIcon} />
            </Button>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

Header.propTypes = {
  logOut: PropTypes.func.isRequired,
  loadUserByPhone: PropTypes.func.isRequired,
  showOneUser: PropTypes.func.isRequired,
  users: PropTypes.array.isRequired
};

export default withStyles(styles)(Header);
