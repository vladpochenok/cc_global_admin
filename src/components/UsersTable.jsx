import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Paper,
  Fab
} from "@material-ui/core";
import { Delete, Info } from "@material-ui/icons";

const styles = theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing.unit * 3,
    overflowX: "auto"
  },
  table: {
    minWidth: 700
  },
  fab: {
    margin: theme.spacing.unit
  }
});

function UsersTable(props) {
  const { classes, openModal, openDeleteModal, users } = props;

  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell>Email</TableCell>
            <TableCell align="right">EmailVerified</TableCell>
            <TableCell align="right">Phone</TableCell>
            <TableCell align="right">Balance</TableCell>
            <TableCell align="right">Currency</TableCell>
            <TableCell align="right">Number of Invites</TableCell>
            <TableCell align="right">Number of Matches</TableCell>
            <TableCell align="right" />
          </TableRow>
        </TableHead>
        <TableBody>
          {users.map(user => {
            return (
              <TableRow key={user.uuid}>
                <TableCell component="th" scope="row">
                  {user.email}
                </TableCell>
                <TableCell align="right">
                  {user.emailVerified ? "true" : "false"}
                </TableCell>
                <TableCell align="right">{user.phone}</TableCell>
                <TableCell align="right">{user.balance}</TableCell>
                <TableCell align="right">{user.currency}</TableCell>
                <TableCell align="right">{user.numberOfInvites}</TableCell>
                <TableCell align="right">{user.numberOfMatches}</TableCell>
                <TableCell align="right">
                  <Fab
                    size="small"
                    color="primary"
                    aria-label="Add"
                    className={classes.fab}
                    onClick={() => openModal(user.phone)}
                  >
                    <Info />
                  </Fab>
                  <Fab
                    size="small"
                    color="secondary"
                    aria-label="Edit"
                    className={classes.fab}
                    onClick={() => openDeleteModal(user.phone)}
                  >
                    <Delete />
                  </Fab>
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </Paper>
  );
}

UsersTable.propTypes = {
  classes: PropTypes.object.isRequired,
  openModal: PropTypes.func.isRequired,
  openDeleteModal: PropTypes.func.isRequired,
  users: PropTypes.array.isRequired
};

export default withStyles(styles)(UsersTable);
