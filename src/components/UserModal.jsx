import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import Modal from "@material-ui/core/Modal";
import { withStyles } from "@material-ui/core/styles";
import {
  ListSubheader,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Collapse,
  IconButton
} from "@material-ui/core";
import {
  MonetizationOn,
  AttachMoney,
  PhoneIphone,
  VerifiedUser,
  AlternateEmail,
  ExpandLess,
  ExpandMore,
  PersonAdd,
  SupervisorAccount,
  Close
} from "@material-ui/icons";

const styles = theme => ({
  paper: {
    position: "absolute",
    width: "500px",
    maxWidth: "100%",
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    outline: "none",
    left: "50%",
    top: "50%",
    transform: "translate(-50%, -50%)"
  },
  root: {
    width: "100%",
    maxWidth: "100%",
    backgroundColor: theme.palette.background.paper
  },
  nested: {
    paddingLeft: theme.spacing.unit * 4
  },
  alignRight: {
    textAlign: "right"
  },
  closeButton: {
    marginLeft: "auto"
  },
  subheader: {
    display: "flex",
    alignItems: "center",
    marginBottom: "15px"
  }
});

class UserModal extends Component {
  state = {
    invitesOpen: false,
    matchesOpen: false
  };

  toggleInvites = () => {
    this.setState(state => ({ invitesOpen: !state.invitesOpen }));
  };

  toggleMatches = () => {
    this.setState(state => ({ matchesOpen: !state.matchesOpen }));
  };

  handleClose = () => {
    const { closeModal } = this.props;

    closeModal();

    this.setState({
      invitesOpen: false,
      matchesOpen: false
    });
  };

  render() {
    const { classes, open, user } = this.props;
    const { invitesOpen, matchesOpen } = this.state;
    const {
      matches,
      invites,
      email,
      emailVerified,
      phone,
      currency,
      balance
    } = user;
    const hasInvites = Boolean(invites && invites.length > 0);
    const hasMatches = Boolean(matches && matches.length > 0);

    return (
      <div>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={open}
          onClose={this.handleClose}
        >
          <div className={classes.paper}>
            <List
              component="div"
              subheader={
                <ListSubheader className={classes.subheader}>
                  <Typography variant="h5" component="h3">
                    User Details
                  </Typography>

                  <IconButton
                    className={classes.closeButton}
                    onClick={this.handleClose}
                  >
                    <Close />
                  </IconButton>
                </ListSubheader>
              }
              className={classes.root}
            >
              <ListItem>
                <ListItemIcon>
                  <AlternateEmail />
                </ListItemIcon>
                <ListItemText inset primary="Email" />
                <ListItemText
                  inset
                  primary={email || ""}
                  className={classes.alignRight}
                />
              </ListItem>
              <ListItem>
                <ListItemIcon>
                  <VerifiedUser />
                </ListItemIcon>
                <ListItemText inset primary="Verified" />
                <ListItemText
                  inset
                  primary={(emailVerified ? "true" : "false") || ""}
                  className={classes.alignRight}
                />
              </ListItem>
              <ListItem>
                <ListItemIcon>
                  <PhoneIphone />
                </ListItemIcon>
                <ListItemText inset primary="Phone" />
                <ListItemText
                  inset
                  primary={phone || ""}
                  className={classes.alignRight}
                />
              </ListItem>
              <ListItem>
                <ListItemIcon>
                  <MonetizationOn />
                </ListItemIcon>
                <ListItemText inset primary="Currency" />
                <ListItemText
                  inset
                  primary={currency || ""}
                  className={classes.alignRight}
                />
              </ListItem>
              <ListItem>
                <ListItemIcon>
                  <AttachMoney />
                </ListItemIcon>
                <ListItemText inset primary="Balance" />
                <ListItemText
                  inset
                  primary={balance || ""}
                  className={classes.alignRight}
                />
              </ListItem>
              {hasInvites && (
                <Fragment>
                  <ListItem button onClick={this.toggleInvites}>
                    <ListItemIcon>
                      <PersonAdd />
                    </ListItemIcon>
                    <ListItemText inset primary="Invited Users" />
                    {invitesOpen ? <ExpandLess /> : <ExpandMore />}
                  </ListItem>
                  <Collapse in={invitesOpen} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                      {invites.map(invitedUser => (
                        <ListItem
                          key={invitedUser.receiver}
                          className={classes.nested}
                        >
                          <ListItemIcon>
                            <PhoneIphone />
                          </ListItemIcon>
                          <ListItemText inset primary="Receiver" />
                          <ListItemText inset primary={invitedUser.receiver} />
                        </ListItem>
                      ))}
                    </List>
                  </Collapse>
                </Fragment>
              )}
              {hasMatches && (
                <Fragment>
                  <ListItem button onClick={this.toggleMatches}>
                    <ListItemIcon>
                      <SupervisorAccount />
                    </ListItemIcon>
                    <ListItemText inset primary="Matches" />
                    {matchesOpen ? <ExpandLess /> : <ExpandMore />}
                  </ListItem>
                  <Collapse in={matchesOpen} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                      {matches &&
                        matches.map(matchedUser => (
                          <ListItem
                            key={matchedUser.with}
                            className={classes.nested}
                          >
                            <ListItemIcon>
                              <PhoneIphone />
                            </ListItemIcon>
                            <ListItemText inset primary="Matched" />
                            <ListItemText inset primary={matchedUser.with} />
                          </ListItem>
                        ))}
                    </List>
                  </Collapse>
                </Fragment>
              )}
            </List>
          </div>
        </Modal>
      </div>
    );
  }
}

UserModal.propTypes = {
  open: PropTypes.bool.isRequired,
  user: PropTypes.object.isRequired,
  closeModal: PropTypes.func.isRequired
};

export default withStyles(styles)(UserModal);
