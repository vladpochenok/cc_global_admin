import React, { Component, Fragment } from "react";
import Header from "components/Header";
import PropTypes from "prop-types";
import { withRouter } from "react-router";
import { compose } from "recompose";
import { withStyles } from "@material-ui/core/styles";
import UsersTable from "components/UsersTable";
import UserTable from "components/UserTable";
import UserModal from "components/UserModal";
import DeleteUserModal from "components/DeleteUserModal";
import { Button } from "@material-ui/core";
import { List } from "@material-ui/icons";

const styles = theme => ({
  progress: {
    margin: theme.spacing.unit * 2
  },
  button: {
    margin: theme.spacing.unit
  },
  rightIcon: {
    marginLeft: theme.spacing.unit
  }
});

class DashboardPage extends Component {
  componentDidMount() {
    const { loadUsers } = this.props;

    loadUsers();
  }

  render() {
    const {
      classes,
      dashboardPage,
      changePhoneValue,
      loadUserByPhone,
      openModal,
      closeModal,
      history,
      logOut,
      deleteUser,
      showOneUser,
      showAllUsers,
      openDeleteModal,
      closeDeleteModal
    } = this.props;
    const {
      users,
      user,
      allUsers,
      phone,
      showUserDetail,
      showDeleteModal,
      selectedUser
    } = dashboardPage;

    return (
      <div className="DashboardPage">
        <Header
          history={history}
          changePhoneValue={changePhoneValue}
          phone={phone}
          loadUserByPhone={loadUserByPhone}
          logOut={logOut}
          showOneUser={showOneUser}
          users={users}
        />
        {allUsers ? (
          <UsersTable
            users={users}
            openModal={openModal}
            openDeleteModal={openDeleteModal}
          />
        ) : (
          <Fragment>
            <Button
              variant="contained"
              color="default"
              className={classes.button}
              onClick={() => showAllUsers()}
            >
              Show all users
              <List className={classes.rightIcon} />
            </Button>
            <UserTable
              user={user}
              openModal={openModal}
              openDeleteModal={openDeleteModal}
            />
          </Fragment>
        )}

        <UserModal
          open={showUserDetail}
          closeModal={closeModal}
          user={user || {}}
        />

        <DeleteUserModal
          open={showDeleteModal}
          closeModal={closeDeleteModal}
          deleteUser={deleteUser}
          selectedUser={selectedUser}
        />
      </div>
    );
  }
}

DashboardPage.propTypes = {
  dashboardPage: PropTypes.object.isRequired,
  loadUserByPhone: PropTypes.func.isRequired,
  openModal: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
  logOut: PropTypes.func.isRequired,
  deleteUser: PropTypes.func.isRequired,
  showOneUser: PropTypes.func.isRequired,
  showAllUsers: PropTypes.func.isRequired,
  openDeleteModal: PropTypes.func.isRequired,
  closeDeleteModal: PropTypes.func.isRequired,
  loadUsers: PropTypes.func.isRequired
};

export default compose(
  withRouter,
  withStyles(styles)
)(DashboardPage);
