import React, { Component } from "react";
import PropTypes from "prop-types";
import LoginForm from "components/LoginForm";
import { withRouter } from "react-router";
import { compose } from "recompose";
import Particles from "react-particles-js";
import "./LoginPage.scss";

class LoginPage extends Component {
  render() {
    const { adminAuthorization, loginPage, history } = this.props;
    return (
      <div className="LoginPage">
        <Particles
          params={{
            particles: {
              number: {
                value: 80
              },
              size: {
                value: 3
              }
            },
            interactivity: {
              events: {
                onhover: {
                  enable: true,
                  mode: "repulse"
                }
              }
            }
          }}
          style={{
            width: "100%",
            height: "100%",
            position: "absolute",
            left: "0px",
            top: "0px"
          }}
        />

        <LoginForm
          authorization={adminAuthorization}
          history={history}
          loginPage={loginPage}
        />
      </div>
    );
  }
}

LoginPage.propTypes = {
  loginPage: PropTypes.object.isRequired,
  adminAuthorization: PropTypes.func.isRequired
};

export default compose(withRouter)(LoginPage);
